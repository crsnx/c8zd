## Инструкция по установке системы мониторинга Zabbix на сервер с Centos 8
В качестве операционной системы сервера была использована популярная ос Centos 8.

Для сбора и анализа информации о состоянии сервера была использована система мониторинга Zabbix 5.4 .
### Этапы установки
1. Копирование репозитория с образами zabbix.
2. Загрузка образов из .tar архивов.
3. Настройка конфигурации docker-compose.yml файла и запуск системы мониторинга.
4. Настройка интерфейса вэб-страницы системы мониторинга.
5. Создание telegram бота.
6. Настройка webhook для отправления уведомлений.
7. Использование

### 1. Копирование репозитория
1. Переходим в домашнюю директорию
2. Копируем репозиторий с помощью команды 
```bash
git clone https://gitlab.com/crsnx/c8zd.git

```
### 2. Загрузка образов 
1. В скопированном репозитории переходим в директорию **~/c8zd/docker_images/**, где находиться 4 архива служб:  
![Screenshot](images_for_instruction.md/1.2.png)
* mysql5.7.tar - База данных необходимая для хранения статитстики сервера.
* zabbix-server-mysql.tar - Сервер, который принмает данные от zabbix-agent и анализирует их.
* zabbix-web-nginx-mysql.tar - Вэб-интерфейс сервера.
* zabbix-agent.tar - Агент, который собирает информацию о системе и передает их zabbix-server.
* net-warden.tar - телеграм бот для отправки статистики по команде и выполнение shell команд

2. Воспользуемся командой docker load для загрузки образов из архивов  
```bash
docker load < mysql5.7.tar ;\
docker load < zabbix-agent.tar ;\
docker load < zabbix-server-mysql.tar ;\
docker load < zabbix-web-nginx-mysql.tar ;\
docker load < net_warden.tar
```  
![Screenshot](images_for_instruction.md/2.1.png)  
3. Выполним команду **docker images** и проверим результат загрузки образов  
![Screenshot](images_for_instruction.md/2.2.png)
### 3. Настройка конфигурации docker-compose.yml файла и запуск системы мониторинга.  
1. Разберем docker-compose.yml по частям 
2. Конфигурация телеграм бота  
   ```yml
   netwarden:
   image: net_warden:latest
   privileged: true
   environment:
      MY_ID: 259555955
      MY_TOKEN: 1963765464:AAEsc_wU58Axv3Iv-U8vO4_5KBt0-9KsRbs
   ```
* Переменные окружения **MY_ID** **MY_TOKEN** передаются в скрипт бота для отправки сообщений.Создание бота подробно описано в 5 разделе инструкции.
* **MY_TOKEN** - токен телеграмм бота (выдается @**BotFather** при создании бота)
* **MY_ID** - ваш id (выдается в @**getmyid_bot** при вводе команды **/start** )

3. Конфигурация mysql-server  
   ```yml
   mysql-server: 
   image: mysql:5.7
   command:
      - mysqld
      - --character-set-server=utf8
      - --collation-server=utf8_bin
   environment:
      MYSQL_DATABASE: zabbix
      MYSQL_USER: pudge
      MYSQL_PASSWORD: pudge_pwd
      MYSQL_ROOT_PASSWORD: root_pwd
   ```
* **MYSQL_DATABASE** - Имя базы данных для zabbix.
* **MYSQL_USER** - Имя пользователя к базе данных MySQL.
* **MYSQL_PASSWORD** - Пароль к базе данных MySQL.
* **MYSQL_ROOT_PASSWORD** - Пароль супер пользователя к базе данных MySQL.

4. Конфигурация zabbix-server  
   ```yml
   zabbix-server-mysql:
   image: zabbix/zabbix-server-mysql:latest
   depends_on:
      - mysql-server
   ports:
      - 10051:10051
   environment:
      DB_SERVER_HOST: mysql-server
      MYSQL_DATABASE: zabbix
      MYSQL_USER: pudge
      MYSQL_PASSWORD: pudge_pwd
      MYSQL_ROOT_PASSWORD: root_pwd
   ```  
* **DB_SERVER_HOST** - Переменная является IP адресом или DNS именем MySQL.
* Экземпляр Zabbix сервера раскрывает 10051/TCP порт на хост машину.

5. Конфигурация вэб-интерфейса  
   ```yml
   zabbix-web-nginx-mysql:
   image: zabbix/zabbix-web-nginx-mysql:latest
   depends_on:
      - mysql-server
      - zabbix-server-mysql
   ports:
      - 80:8080
   environment:
      DB_SERVER_HOST: mysql-server
      MYSQL_DATABASE: zabbix
      MYSQL_USER: pudge
      MYSQL_PASSWORD: pudge_pwd
      MYSQL_ROOT_PASSWORD: root_pwd
   ```
* Экземпляр Zabbix веб-интерфейс раскрывает 80/TCP порт (HTTP) на хост машину.

6. Конфигурация zabbix-agent
   ```yml
   zabbix-agent:
   image: zabbix/zabbix-agent:latest
   privileged: true
   depends_on:
      - zabbix-server-mysql
   environment:
      ZBX_HOSTNAME: ArcWarden
      ZBX_SERVER_HOST: zabbix-server-mysql
   ```
* **ZBX_HOSTNAME** - Имя zabbix агента.
* **ZBX_SERVER_HOST** - Переменная является IP адресом или DNS именем сервера Zabbix.

## Заменяем переменные окружения на свои и запускаем docker-compose.
Выполняем команду в папке с файлом docker-compose.yml
   ```bash
   dokcer-compose up -d
   ```
 Нам потребуется ip адрес Zabbix агента. Для этого нужно выполнить команду ifconfig в контейнере.
   ```bash
   docker exec -it "ID КОНТЕЙНЕРА ZABBIX AGENT" /bin/bash	    
   ```
Далее выполняем в контейнере команду **ifconfig**, копируем ip адрес (Понадобится в 4.3), выходим из контейнера командой **exit**  
![Screenshot](images_for_instruction.md/3.5.png)

## 4. Настройка вэб-интерфейса
1. В браузере вводим ip адрес сервера. **Логин- Admin Пароль- zabbix**
(скрин экрана авторизации)

![Screenshot](images_for_instruction.md/4.png)

2. Первое, что нужно сделать после установки - сменить стандартные учетные данные для входа. Для этого идём в раздел Administration -> Users -> Admin.

![Screenshot](images_for_instruction.md/4.2.png)

Изменяем логин, пароль, язык и часовой пояс

![Screenshot](images_for_instruction.md/4.2.1.png)

3. Необходимо задать корректный ip адрес контейнера Zabbix агента, который мы получили в шаге 3.5 . Для этого идём в раздел Мониторинг -> Узлы сети -> Zabbix Server -> Настройка
![Screenshot](images_for_instruction.md/4.3.png)
В поле Интерфейсы-Агент устанавливаем ip который получили из контейнера zabbix-agent.
![Screenshot](images_for_instruction.md/4.4.png)
Спустя несколько минут zabbix server получит пакеты от агента и обновиться.
![Screenshot](images_for_instruction.md/4.5.png)

## 5. Создание telegram бота
1. Создадим бота, через которого будем слать оповещения из заббикса. Для этого добавляем себе в контакты @**BotFather** и пишем ему сначала **/start**, потом **/newbot**.

![Screenshot](images_for_instruction.md/5.1.png)

2. Так же нам необходимо узнать id нашего аккаунта. Для этого добавляем себе в контакты @**getmyid_bot** и пишем ему команду **/start**

![Screenshot](images_for_instruction.md/5.2.png)

### 6. Настройка webhook для отправления уведомлений

1. Чтобы настроить webhook, переходите в Администрирование -> Способы оповещений и выбирайте там Telegram.
![Screenshot](images_for_instruction.md/6.1.png)

2. Внутри нужно установить только токен телеграм бота
![Screenshot](images_for_instruction.md/6.2.png)

3. Теперь проверим отправку уведомлений в телеграм. Для этого в разделе 'Способы оповещений' нажмите кнопку тест рядом с telegram

![Screenshot](images_for_instruction.md/6.3.png)

В полях 1- Заголовок сообщения 2- Сообщение 3- Ваш id

![Screenshot](images_for_instruction.md/6.4.png)

Итог теста:

![Screenshot](images_for_instruction.md/6.5.png)

4. Теперь идем в Администрирование-> Пользователи-> "Логин Админа"-> Оповещения -> Добавить и добавляем ему способ оповещений через telegram и указываем свой id. Добавляем -> Обновляем.

![Screenshot](images_for_instruction.md/6.6.png)

5. Необходимо активировать уведомления. Для этого в активируем службу уведомлений в Настройки-> Действия -> Действия тригеров

![Screenshot](images_for_instruction.md/6.8.png)

**Настройка системы мониторинга завершена**

### Использование
Вэб-сервер zabbix позволяет просматривать состояния системы в любой момент и предосталяет временные графики для просмотра загруженности сервера в любое время. Так же ситема имеет набор стандартных тригеров, которые срабатывают при каком-либо проишествии(Например в случае скорого заполнения дискового пространства). При срабатывании тригера незамедлительно следует уведомление в бота телеграмм, что позволяет оперативно реагировать на проишествие.

**Графики** находятся по пути Мониторинг -> Узлы сети -> Графики

**Данные о использовании ресурсов** находятся по пути Мониторинг -> Обзор -> Обзор Данных

Телеграм бот имеет 2 команды:

* **/stats** - выведет статистику сервера (Аптайм,загруженность процессора, оперативки, диска)

* **/shell** - позволяет выполнить переданную команду на сервере (например ls) и получить результат выполнения программы в чате 
