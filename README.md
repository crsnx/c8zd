# Centos8_Zab_D
Задание по мониторингу сервера Линукс.

В папке **docker_images** хранятся архивы образов служб Zabbix.

В папке **docker_cmp** находится docker-compose файл для быстрого разворачиваия проекта.

В папке **t_bot** находится исходный код телеграм бота для получения статистики сервера.

В **instruction_docker-run.md** описан метод установки системы мониторинга с помощью docker контейнеров.

В **instruction_docker-compose.md** описан метод установки системы монитронига с помощью docker-compose.

Папка images_for_instruction.md хранит скриншоты для instruction.md
