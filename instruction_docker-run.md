## Инструкция по установке системы мониторинга Zabbix на сервер с Centos 8
В качестве операционной системы сервера была использована популярная ос Centos 8.

Для сбора и анализа информации о состоянии сервера была использована система мониторинга Zabbix 5.4 .
### Этапы установки
1. Копирование репозитория с образами zabbix.
2. Загрузка образов из .tar архивов.
3. Поэтапный запуск и связывание контейнеров системы мониторинга.
4. Настройка интерфейса вэб-страницы системы мониторинга.
5. Создание telegram бота.
6. Настройка webhook для отправления уведомлений.
7. Запуск контейнера с telegram ботом для получения статистики
8. Использование

### 1. Копирование репозитория
1. Переходим в домашнюю директорию
2. Копируем репозиторий с помощью команды 
```bash
git clone https://gitlab.com/crsnx/c8zd.git

```
### 2. Загрузка образов 
1. В скопированном репозитории переходим в директорию **~/c8zd/docker_images/**, где находиться 4 архива служб:(Скрин)
![Screenshot](images_for_instruction.md/1.2.png)
* mysql5.7.tar - База данных необходимая для хранения статитстики сервера.
* zabbix-server-mysql.tar - Сервер, который принмает данные от zabbix-agent и анализирует их.
* zabbix-web-nginx-mysql.tar - Вэб-интерфейс сервера.
* zabbix-agent.tar - Агент, который собирает информацию о системе и передает их zabbix-server.
* net-warden.tar - телеграм бот для отправки статистики по команде и выполнение shell команд

2. Воспользуемся командой docker load для загрузки образов из архивов
```bash
docker load < mysql5.7.tar ;\
docker load < zabbix-agent.tar ;\
docker load < zabbix-server-mysql.tar ;\
docker load < zabbix-web-nginx-mysql.tar ;\
docker load < net_warden.tar
```
![Screenshot](images_for_instruction.md/2.1.png)
3. Выполним команду **docker images** и проверим результат загрузки образов (Скрин)
![Screenshot](images_for_instruction.md/2.2.png)
### 3. Развертывание системы мониторинга
**!Здесь важен порядок запусков контейнеров!**
1. Запускаем пустой экземпляр MySQL сервера. Переменные окружения стоят по умолчанию - для безопасности установите свои и запомните их!
```bash
docker run --name mysql-server -t \
      -e MYSQL_DATABASE="zabbix" \
      -e MYSQL_USER="pudge" \
      -e MYSQL_PASSWORD="pudge_pwd" \
      -e MYSQL_ROOT_PASSWORD="root_pwd" \
      -d mysql:5.7 \
      --character-set-server=utf8 --collation-server=utf8_bin
```
* **MYSQL_DATABASE** - Имя базы данных для zabbix.
* **MYSQL_USER** - Имя пользователя к базе данных MySQL.
* **MYSQL_PASSWORD** - Пароль к базе данных MySQL.
* **MYSQL_ROOT_PASSWORD** - Пароль супер пользователя к базе данных MySQL.

2. Запускаем экземпляр Zabbix сервера, открываем служебный порт 10051 и соединяем этот экземпляр с созданным экземпляром MySQL сервера.

**Значения переменных MYSQL_(DATABASE,USER,PASSWORD,ROOT_PASSWORD) берем из прошолой команды.**
```bash
docker run --name zabbix-server-mysql -t \
      -e DB_SERVER_HOST="mysql-server" \
      -e MYSQL_DATABASE="zabbix" \
      -e MYSQL_USER="pudge" \
      -e MYSQL_PASSWORD="pudge_pwd" \
      -e MYSQL_ROOT_PASSWORD="root_pwd" \
      --link mysql-server:mysql \
      -p 10051:10051 \
      -d zabbix/zabbix-server-mysql:latest
```
* **DB_SERVER_HOST** - Переменная является IP адресом или DNS именем MySQL.
* Экземпляр Zabbix сервера раскрывает 10051/TCP порт на хост машину.

3. Запускаем Zabbix веб-интерфейс и соединяем этот экземпляр с недавно созданными экземплярами MySQL сервера и Zabbix сервера

```bash
 docker run --name zabbix-web-nginx-mysql -t \
      -e DB_SERVER_HOST="mysql-server" \
      -e MYSQL_DATABASE="zabbix" \
      -e MYSQL_USER="pudge" \
      -e MYSQL_PASSWORD="pudge_pwd" \
      -e MYSQL_ROOT_PASSWORD="root_pwd" \
      --link mysql-server:mysql \
      --link zabbix-server-mysql:zabbix-server \
      -p 80:8080 \
      -d zabbix/zabbix-web-nginx-mysql:latest
```
* Экземпляр Zabbix веб-интерфейс раскрывает 80/TCP порт (HTTP) на хост машину.

4. Запускаем Zabbix agent и соединяем его с экземпляром сервера Zabbix
```bash
 docker run --name zabbix-agent \
    -e ZBX_HOSTNAME="Net Warden" \
    -e ZBX_SERVER_HOST="zabbix-server-mysql" \
    --link zabbix-server-mysql:zabbix-server \
    --privileged \
    -d zabbix/zabbix-agent:latest	    
```
* **ZBX_HOSTNAME** - Имя zabbix агента.
* **ZBX_SERVER_HOST** - Переменная является IP адресом или DNS именем сервера Zabbix.

5. Нам потребуется ip адрес Zabbix агента. Для этого нужно выполнить команду ifconfig в контейнере.
```bash
 docker exec -it "ID КОНТЕЙНЕРА ZABBIX AGENT" /bin/bash	    
```
Далее выполняем в контейнере команду **ifconfig**, копируем ip адрес (Понадобится в 4.3), выходим из контейнера командой **exit**
![Screenshot](images_for_instruction.md/3.5.png)

## 4. Настройка вэб-интерфейса
1. В браузере вводим ip адрес сервера. **Логин- Admin Пароль- zabbix**
(скрин экрана авторизации)

![Screenshot](images_for_instruction.md/4.png)

2. Первое, что нужно сделать после установки - сменить стандартные учетные данные для входа. Для этого идём в раздел Administration -> Users -> Admin.

![Screenshot](images_for_instruction.md/4.2.png)

Изменяем логин, пароль, язык и часовой пояс

![Screenshot](images_for_instruction.md/4.2.1.png)

3. Необходимо задать корректный ip адрес контейнера Zabbix агента, который мы получили в шаге 3.5 . Для этого идём в раздел Мониторинг -> Узлы сети -> Zabbix Server -> Настройка
![Screenshot](images_for_instruction.md/4.3.png)
В поле Интерфейсы-Агент устанавливаем ip который получили из контейнера zabbix-agent.
![Screenshot](images_for_instruction.md/4.4.png)
Спустя несколько минут zabbix server получит пакеты от агента и обновиться.
![Screenshot](images_for_instruction.md/4.5.png)

## 5. Создание telegram бота
1. Создадим бота, через которого будем слать оповещения из заббикса. Для этого добавляем себе в контакты @**BotFather** и пишем ему сначала **/start**, потом **/newbot**.

![Screenshot](images_for_instruction.md/5.1.png)

2. Так же нам необходимо узнать id нашего аккаунта. Для этого добавляем себе в контакты @**getmyid_bot** и пишем ему команду **/start**

![Screenshot](images_for_instruction.md/5.2.png)

### 6. Настройка webhook для отправления уведомлений

1. Чтобы настроить webhook, переходите в Администрирование -> Способы оповещений и выбирайте там Telegram.
![Screenshot](images_for_instruction.md/6.1.png)

2. Внутри нужно установить только токен телеграм бота
![Screenshot](images_for_instruction.md/6.2.png)

3. Теперь проверим отправку уведомлений в телеграм. Для этого в разделе 'Способы оповещений' нажмите кнопку тест рядом с telegram

![Screenshot](images_for_instruction.md/6.3.png)

В полях 1- Заголовок сообщения 2- Сообщение 3- Ваш id

![Screenshot](images_for_instruction.md/6.4.png)

Итог теста:

![Screenshot](images_for_instruction.md/6.5.png)

4. Теперь идем в Администрирование-> Пользователи-> "Логин Админа"-> Оповещения -> Добавить и добавляем ему способ оповещений через telegram и указываем свой id. Добавляем -> Обновляем.

![Screenshot](images_for_instruction.md/6.6.png)

5. Необходимо активировать уведомления. Для этого в активируем службу уведомлений в Настройки-> Действия -> Действия тригеров

![Screenshot](images_for_instruction.md/6.8.png)

**Настройка системы мониторинга завершена**

### Контейнер с телеграм ботом 

1. Мы уже загрузили образ бота и у нас есть этот образ на серевере. Теперь необходимо выполнить команду по запуску контейнера
```bash
 docker run --name net_warden_bot \
	-e MY_ID=259555955 \
	-e MY_TOKEN=2010152408:AAH3c4RTNlzAhcbblYKinaFQHEcPFT6tTW8 \
	--privileged \
	-d net_warden:latest	    
```
* MY_ID - ваш chat_id
* MY_TOKEN - ваш токен телеграм бота 
* Важно! Вставляйте значения без скобок ( (), [], '', "" )

### Использование
Вэб-сервер zabbix позволяет просматривать состояния системы в любой момент и предосталяет временные графики для просмотра загруженности сервера в любое время. Так же ситема имеет набор стандартных тригеров, которые срабатывают при каком-либо проишествии(Например в случае скорого заполнения дискового пространства). При срабатывании тригера незамедлительно следует уведомление в бота телеграмм, что позволяет оперативно реагировать на проишествие.

**Графики** находятся по пути Мониторинг -> Узлы сети -> Графики

**Данные о использовании ресурсов** находятся по пути Мониторинг -> Обзор -> Обзор Данных

Телеграм бот имеет 2 команды:

* **/stats** - выведет статистику сервера (Аптайм,загруженность процессора, оперативки, диска)

* **/shell** - позволяет выполнить переданную команду на сервере (например ls) и получить результат выполнения программы в чате 
